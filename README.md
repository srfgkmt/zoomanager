# ZooManager

ZooManager is a Tool to manage a ZooKeeper Tree and Nodekeeper Rules. At the moment Zookeeper supports:

* [Manipulating a ZooKeeper tree](#markdown-header-tree-view)
* [Add, alter and remove NodeKeeper rules](#markdown-header-rules-view)
* [Boostrap a ZooKeeper tree with a file](#markdown-header-bootstrap-view)

[Here you get more information about technical details and installation.](#markdown-header-technical-details).

## Tree View

![Figure 1: Tree View](https://bytebucket.org/srfgkmt/zoomanager/raw/master/doc/img/screenshot1.png "Figure 1: Tree View")

Figure 1 shows the tree view. You can browse the tree (like in a file structure) on the left and edit node data
of existing nodes on the right. To edit an existing node, click on the node, edit it on the right, and save the changes.
To add a e.g. childnode on a node **zookeeper**, click on **zookeeper** in the tree, than click on **+**. A modal dialog
appears (Figure 2), where you can set the name and the value of the node.

![Figure 2: Modal Dialog](https://bytebucket.org/srfgkmt/zoomanager/raw/master/doc/img/screenshot2.png "Figure 2: Modal Dialog")

## Rules View

In the Rule View you can add new rules. How rules has to be specified in xml is described on
[the NodeKeeper repository](https://github.com/tkurz/nodekeeper#event-binding-action-rules). Don't forget, that a full rules
xml structure must be provided. It has to start with a root node `<rules>`! Figure 3 shows a screenshot of the Rule View
including an existing rule (name my_rule) and a new rule that is prepared for submission.

![Figure 3: Rule View](https://bytebucket.org/srfgkmt/zoomanager/raw/master/doc/img/screenshot3.png "Figure 2: Rule View")

To get a closer look at a already existing rule, click on the label (**my_rule** on the screenshot). A textbox appears
where you can edit the rule. To save your changes, click on **save**. To delete a rule click on **remove**.

## Bootstrap View

In this view you can boostrap a zookeeper tree with a file. The file contains simple name value pairs, like:

```
my/nodes/node1 = data1
my/nodes/node2 = data2
my/other/nodes/n = {"name":"n","value":"123"}

```

The path are created recursively which means that you do not have to specify all parent nodes separately (of course the
data of parent nodes is than empty). If you enable **clean zookeeper**, all existing nodes are deleted. Be careful with this!
In the next version you will also be able to export the existing tree to a file.

# Technical Details

Here you get more information about software components and installation.

## Install ZooManager

ZooManager MUST be deployed in a Apache Tomcat web container, because it is uses WebSocketServlet services
for the backend-frontend communication. On startup, you can configure the home directory **zoomanager.home** , the ZooKeeper
connection string **zoomanager.host** and the ZooKeeper timeout in milliseconds **zoomanager.timeout.**. The default values are:

name  | value
------------- | -------------
zoomanager.home  | /tmp/zoomanager
**zoomanager.host  | localhost:2181
zoomanager.timeout | 60000

The standard configuration within the Redlink cluster is:

```xml
<Context docBase="/data/instances/zoomanager/zoomanager.war" unpackWAR="false" useNaming="true">
  <Parameter name="zoomanager.home" value="/data/zoomanager" override="false" />
  <Parameter name="zoomanager.timeout" value="5000" override="false" />
  <Parameter name="zoomanager.host" value="zookeeper01,zookeeper02,zookeeper03,zookeeper04,zookeeper05" override="false" />
</Context>
```

## ZooManager Backend
The backend is implemented in Java. The web application is a *javax.ws.rs.core.Application*. ZooManager mainly wraps
[NodeKeeper](https://github.com/tkurz/nodekeeper) and its functionalities with webservices and websockets.

### NKNodeWebservice
This webservice allows to list, create, read, update and delete ZooKeeper nodes.

#### listNodes:
List all immediate children nodes for a given path.

**URL**: /service/zookeeper/node/list

**METHOD**: GET

**PARAMETER**:

name  | value
------------- | -------------
path | the path of the parent node
clazz | Java class that is used to parse the data. (Default is "java.lang.String")



#### createNode:
Create node on a given path. Parent nodes are created recursively if necessary.

**URL**: /service/zookeeper/node

**METHOD**: POST

**PARAMETER**:

name  | value
------------- | -------------
path | the path of the parent node
clazz | Java class that is used to parse the data. (Default is "java.lang.String")
BODY | the data



#### readNode:
Read node on a given path.

**URL**: /service/zookeeper/node

**METHOD**: GET

**PARAMETER**:

name  | value
------------- | -------------
path | the path of the parent node
clazz | Java class that is used to parse the data. (Default is "java.lang.String")



#### updateNode:
Update node on a given path.

**URL**: /service/zookeeper/node

**METHOD**: PUT

**PARAMETER**:

name  | value
------------- | -------------
path | the path of the parent node
clazz | Java class that is used to parse the data. (Default is "java.lang.String")
BODY | the data



#### deleteNode:
Delete node on a given path. Children nodes are deleted recursively.

**URL**: /service/zookeeper/node

**METHOD**: DELETE

**PARAMETER**:

name  | value
------------- | -------------
path | the path of the parent node



### NKRuleWebservice
This webservice allows to add, edit and remove NodeKeeper rules.

#### createRules:
Create NodeKeeper rules.

**URL**: /service/nodekeeper/rules

**METHOD**: POST

**PARAMETER**:

name  | value
------------- | -------------
BODY | the rule in xml syntax




#### readRules:
Read NodeKeeper rules.

**URL**: /service/nodekeeper/rules

**METHOD**: GET

**PARAMETER**:

none




#### updateRules:
Update NodeKeeper rules.

**URL**: /service/nodekeeper/rules

**METHOD**: PUT

**PARAMETER**:

name  | value
------------- | -------------
BODY | the rule in xml syntax




#### getSingleRule:
Update NodeKeeper rules.

**URL**: /service/nodekeeper/rule

**METHOD**: GET

**PARAMETER**:

name  | value
------------- | -------------
id | the rule id




#### deleteSingleRule:
Update NodeKeeper rules.

**URL**: /service/nodekeeper/rule

**METHOD**: DELETE

**PARAMETER**:

name  | value
------------- | -------------
id | the rule id



### NKBoostrapWebservice
For ZooKeeper bootstrapping.

#### uploadTree:
Uploads a tree file and created tree.

**URL**: /service/zookeeper/bootstrap

**METHOD**: POST

**PARAMETER**:

name  | value
------------- | -------------
file | the file url
clean | if the existing tree should be deleted. (Default is *true*)
