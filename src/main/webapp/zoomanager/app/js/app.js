'use strict';


// Declare app level module which depends on filters, and services
angular.module('ZooManager', [
  'ngRoute',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers',
  'angularFileUpload'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/tree', {templateUrl: 'views/tree.html', controller: 'TreeController', activetab: 'tree'});
  $routeProvider.when('/rules', {templateUrl: 'views/rules.html', controller: 'RuleController', activetab: 'rules'});
  $routeProvider.when('/bootstrap', {templateUrl: 'views/bootstrap.html', controller: 'BootstrapController', activetab: 'bootstrap'});
  $routeProvider.otherwise({redirectTo: '/tree'});
}]).
    directive('ngRightClick', function ($parse) {
        return function (scope, element, attrs) {
            var fn = $parse(attrs.ngRightClick);
            element.bind('contextmenu', function (event) {
                scope.$apply(function () {
                    event.preventDefault();
                    fn(scope, {$event:event});
                });
            });
        };
    });
