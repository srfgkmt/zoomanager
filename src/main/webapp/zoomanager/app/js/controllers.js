'use strict';

/* Controllers */

angular.module('myApp.controllers', ['ui.bootstrap']).
    controller("TreeController", ['$scope', '$http', '$modal','$log','$timeout','$rootScope', function ($scope, $http, $modal, $log, $timeout, $rootScope) {

    var root = 'ROOT';
    var defaultType = 'String'

    var templates = {
        "java.lang.String":"views/data/stringData.html"//,
        //"java.lang.Boolean":"views/data/booleanData.html",
        //"java.util.Dictionary":"views/data/dictionary.html",
        //"DEFAULT":"views/data/stringData.html"
    }

    var defaultHandler = {name:"String",value:"java.lang.String",template:templates['java.lang.String']};

    //basic tree functions
    $scope.tree = {}
    $scope.tree[root] = {"path":"/", "data":"", "version":0, "nbOfChildren":1, "clazz":"java.lang.String",children:{}};
    $scope.handlers = [defaultHandler];
    $scope.handler = $scope.handlers[0];

    //load handlers
    /*$http.get($rootScope.base + 'service/zookeeper/node/handlers').success(function(data){
        $scope.handlers = data;
        for(var i in $scope.handlers) {
            if($scope.handlers[i].name == defaultType) {
                $scope.handler = $scope.handlers[i];
                defaultHandler = $scope.handlers[i];
                break;
            }
        }
    });
    */

    $scope.activeNode = undefined;

    $scope.activate = function(node) {
        $http.get($rootScope.base + 'service/zookeeper/node?path=' + encodeURIComponent(node.path) + "&clazz=" + encodeURIComponent($scope.handler.value)).success(function (data) {
            $scope.activeNode = data;
            $scope.activeNode.template = templates[$scope.handler.value] != undefined ? templates[$scope.handler.value] : templates['DEFAULT'];
            $scope.$watch('activeNode.data',function(a,b) {
               if(a != b && $scope.activeNode) $scope.activeNode.changed = true;
            });
        }).error(function(){
            alert("cannot read node "+$scope.activeNode.path+" as "+$scope.handler.name);
            $scope.handler = defaultHandler;
        });
    }

    $scope.$watch('handler',function(o,n){
        if(o.value != n.value) $scope.activate($scope.activeNode);
    },true);

    $scope.openChildren = function (node) {
        node.childrenOpen = true;
        node.childrenLoading = true;
        $http.get($rootScope.base + 'service/zookeeper/node/list?path=' + encodeURIComponent(node.path)).success(function (data) {
            node.childrenLoading = false;
            var length = 0;
            for (var i in data) {
                data[i].children = {};length++;
            }
            node.children = data;
            node.nbOfChildren = length;
        });
    };
    $scope.closeChildren = function (node) {
        node.childrenOpen = false;
        node.children = [];
    };

    $scope.makeEditable = function(data) {
        data.editable = true;
        $timeout(function() {
            data.editable = false;
        },3000);
    }

    $scope.addChildren = function(data) {
        var modalInstance = $modal.open({
            templateUrl: 'addNodeModal.html',
            controller: function ($scope, $modalInstance) {
                $scope.path = data.path;
                $scope.input = {};

                $scope.ok = function () {
                    $modalInstance.close($scope.input);
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });

        modalInstance.result.then(function (input) {
            var path = data.path == "/" ? "/" + input.label : data.path + "/" + input.label;
            $http({method: 'POST',
                url: $rootScope.base + 'service/zookeeper/node?path=' + encodeURIComponent(path) + "&clazz=" + encodeURIComponent($scope.handler.value),
                data: input.data,
                headers: {'Content-Type': 'text/plain'}
            }).success(function(data){
                $scope.activate(data);
            })
        });
    };
    $scope.removeNode = function(data) {
        if(!confirm("delete node "+data.path + (data.nbOfChildren == 0 ? "" : " AND subnodes"))) return;
        $http({method: 'DELETE', url: $rootScope.base + 'service/zookeeper/node?path=' + encodeURIComponent(data.path)}).success(function(){
            if($scope.activeNode == data) $scope.activeNode = undefined;
        })
    };

    $scope.saveNode = function() {
        $http({method: 'PUT',
            url: $rootScope.base + 'service/zookeeper/node?path=' + encodeURIComponent($scope.activeNode.path) + "&clazz=" + encodeURIComponent($scope.handler.value),
            data: $scope.activeNode.data,
            headers: {'Content-Type': 'text/plain'}
        });
    }

    //websocket
    var wevSocketIsActive = false;
    var webSocket = new WebSocket($rootScope.websocket);

    function findParent(path) {
        var queue = path.split("/");
        var query = "$scope.tree[\""+root+"\"]";
        for(var i = 1; i < queue.length-1; i++) {
            query += ".children[\""+queue[i]+"\"]";
        }
        return eval(query);
    }

    var run = {
        'created':function(uri) {
            var node = findParent(uri);
            if(!node) return;
            if(node.childrenOpen) {
                $http.get($rootScope.base + 'service/zookeeper/node?path=' + encodeURIComponent(uri) + "&clazz=" + encodeURIComponent($scope.handler.value)).success(function (data) {
                    data.children = {};
                    node.children[uri.substring(uri.lastIndexOf("/")+1)] = data;
                }).error(function(){
                        alert("cannot read node "+$scope.activeNode.path+" as "+$scope.handler.name);
                        $scope.handler = defaultHandler;
                });
            };
            node.nbOfChildren += 1;
            $scope.$apply();
        },
        'updated':function(uri) {
            if($scope.activeNode && $scope.activeNode.path == uri) {
                $http.get($rootScope.base + 'service/zookeeper/node?path=' + encodeURIComponent(uri) + "&clazz=" + encodeURIComponent($scope.handler.value)).success(function (data) {
                    $scope.activeNode = data;
                    $scope.activeNode.template = templates[$scope.handler.value] != undefined ? templates[$scope.handler.value] : templates['DEFAULT'];
                }).error(function(){
                    alert("cannot read node "+$scope.activeNode.path+" as "+$scope.handler.name);
                    $scope.handler = defaultHandler;
                });;
            }
        },
        'deleted':function(uri) {
            var node = findParent(uri);
            if(node.childrenOpen) {
                var child = node.children[uri.substring(uri.lastIndexOf("/")+1)];
                if($scope.activeNode && $scope.activeNode.path == child.path) $scope.activeNode = undefined;
                delete node.children[uri.substring(uri.lastIndexOf("/")+1)];
            }
            node.nbOfChildren -= 1;
            $scope.$apply();
        }
    }

    webSocket.onmessage = function(event) {
        if(wevSocketIsActive) run[event.data.substring(0,7)](event.data.substring(8));
        else if(event.data == "activate") wevSocketIsActive = true;
    };

}])
    .controller("RuleController", ['$scope', '$http', '$rootScope','$log','$timeout','$sce', function ($scope, $http, $rootScope, $log, $timeout, $sce) {

        $scope.loading = true;
        $scope.newRules = "<rules>\n\n</rules>";
        $scope.ruleList = [];

        $scope.save = function(rule) {
            $http({method: 'PUT',
                url: $rootScope.base + "service/nodekeeper/rules",
                data: rule.data,
                headers: {'Content-Type': 'application/xml'}
            }).success(function(){
                alert("saved succesfully");
            });
        }

        $scope.remove = function(rule) {
            $http({method: 'DELETE',
                url: $rootScope.base + "service/nodekeeper/rule?id="+rule.id
            }).success(function(){
                    for(var i = 0; i < $scope.ruleList.length;i++) {
                        if($scope.ruleList[i].id == rule.id) {console.log(i);
                            $scope.ruleList.splice(i, 1);break;
                        }
                    }
            });
        }

        function loadSingle(id,description) {
            $http({method: 'GET',
                url: $rootScope.base + "service/nodekeeper/rule?id="+id,
                headers: {'Accept': 'application/xml'}
            }).success(function(data){
                $scope.ruleList.push({id:id,data:vkbeautify.xml(data),description:$sce.trustAsHtml(description),show:false});
            })
        }

        function load() {
            $scope.ruleList = [];
            $http({method: 'GET',
                url: $rootScope.base + "service/nodekeeper/rule-ids",
                headers: {'Accept': 'application/json'}
            }).success(function(data){
                for(var i in data) {
                    loadSingle(i,data[i]);
                }
                $scope.loading = false;
            })
        }

        load();

        $scope.upload = function() {
            $http({method: 'PUT',
                url: $rootScope.base + "service/nodekeeper/rules",
                data: $scope.newRules,
                headers: {'Content-Type': 'application/xml'}
            }).success(function(){
                    load();
                    alert("saved successfully");
                }).error(function(){
                    alert("could not store rules");
                })
        }

        $scope.downloadUrl = $rootScope.base + "service/nodekeeper/rules.xml";

        /*
        $scope.rules = "...loading";

        function load() {
            $http({method: 'GET',
                url: $rootScope.base + "service/nodekeeper/rule",
                headers: {'Accept': 'application/xml'}
            }).success(function(data){
                $scope.rules = vkbeautify.xml(data);
            })
        }
        load();


        */

}]).controller("BootstrapController",['$scope','$http','$rootScope','$upload',function($scope,$http,$rootScope,$upload) {

        $scope.filename = "/tmp/bootstrap.properties";
        $scope.clean = false;

        $scope.load = function() {
            $http.post($rootScope.base + 'service/zookeeper/bootstrap/upload?file='+$scope.filename + "&clean=" + $scope.clean).success(function(){
                alert("bootstrap was successfull");
            })
        }

        /*$scope.onFileSelect = function($files) {
            //$files: an array of files selected, each file has name, size, and type.
            for (var i = 0; i < $files.length; i++) {
                var $file = $files[i];
                $scope.upload = $upload.upload({
                    url: $rootScope.base + 'service/zookeeper/bootstrap/upload', //upload.php script, node.js route, or servlet url
                    // method: POST or PUT,
                    // headers: {'headerKey': 'headerValue'}, withCredential: true,
                    //data: {myObj: $scope.myModelObj},
                    fileFormDataName: "file"
                    //formDataAppender: function(formData, key, val){}
                }).progress(function(evt) {
                        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                    }).success(function(data, status, headers, config) {
                        // file is uploaded successfully
                        console.log(data);
                    });
                //.error(...)
                //.then(success, error, progress);
            }
        };*/

}]).run(function ($rootScope, $route) {
    //get host
    var host = location.host;//"worker1.bb.redlink.io:8080"
    $rootScope.folder = 'zoomanager'
    $rootScope.base = 'http://'+host+'/'+$rootScope.folder+'/'; //global variable
    $rootScope.websocket = 'ws://'+host+'/'+$rootScope.folder+'/websocket';
    $rootScope.$route = $route;
});;