'use strict';

/* Filters */

angular.module('myApp.filters', []).
  filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/\%VERSION\%/mg, version);
    }
  }]).filter('truncate', function () {
    return function (text, length1, length2) {
        if (isNaN(length1))
            length1 = 10;

        if (isNaN(length2))
            length2 = 10;

        var end = "...";

        if (text.length < length1+length2+1) {
            return text;
        }
        else {
            return String(text).substr(0,length1) + end + String(text).substring(text.length-(length2));
        }
    };
});
