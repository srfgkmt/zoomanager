package at.salzburgresearch.zookeeper.manager;

import at.salzburgresearch.zookeeper.manager.handler.DictionaryHandler;
import at.salzburgresearch.zookeeper.manager.utils.Constants;
import at.salzburgresearch.zookeeper.manager.webservice.NKRuleWebservice;
import at.salzburgresearch.zookeeper.manager.webservice.ZKBootstrapWebservice;
import at.salzburgresearch.zookeeper.manager.webservice.ZKNodeWebService;
import at.salzburgresearch.nodekeeper.NodeKeeper;
import at.salzburgresearch.nodekeeper.exception.NodeKeeperException;

import javax.ws.rs.core.Application;
import java.io.IOException;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class StanbolManagerApplication extends Application {

    //TODO store when shutdown
    private Properties properties;

    private static final String RULES_FILE = "rules.xml";

    public NodeKeeper nodeKeeper;

    public Set<Object> singletons = new HashSet<Object>();

    public StanbolManagerApplication() throws IOException {

        properties = Constants.NodeKeeperProperties;

        try {
            nodeKeeper = new NodeKeeper(Constants.ZKConnectionString,Constants.ZKTimeout,properties);
            nodeKeeper.addDataHandler(new DictionaryHandler());
            singletons.add(new ZKNodeWebService(nodeKeeper));
            singletons.add(new NKRuleWebservice(nodeKeeper));
            singletons.add(new ZKBootstrapWebservice(nodeKeeper));

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        } catch (NodeKeeperException e) {
            e.printStackTrace();
        }
    }

    public Set<Object> getSingletons() {
        return singletons;
    }
}
