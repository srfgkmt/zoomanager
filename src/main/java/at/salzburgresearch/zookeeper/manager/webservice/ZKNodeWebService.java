package at.salzburgresearch.zookeeper.manager.webservice;

import at.salzburgresearch.zookeeper.manager.model.NodeWrapper;
import at.salzburgresearch.nodekeeper.NodeKeeper;
import at.salzburgresearch.nodekeeper.exception.NodeKeeperException;
import at.salzburgresearch.nodekeeper.handlers.DataHandler;
import at.salzburgresearch.nodekeeper.model.Node;
import com.google.common.base.Preconditions;
import org.apache.commons.io.IOUtils;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
@Path("/service/zookeeper/node")
public class ZKNodeWebService {

    NodeKeeper nodeKeeper;

    public ZKNodeWebService(NodeKeeper nodeKeeper) {
        this.nodeKeeper = nodeKeeper;
    }

    @Path("/handlers")
    @Produces("application/json")
    @GET
    public Response listTypes() {
        List<DataHandler> handlers = nodeKeeper.listDataHandlers();
        List<HashMap<String,String>> handler_types = new ArrayList<HashMap<String,String>>();
        for(final DataHandler handler : handlers) {
            handler_types.add(new HashMap<String, String>(){{put("name",handler.getType().getSimpleName());put("value",handler.getType().getName());}});
        }
        return Response.ok().entity(handler_types).build();
    }

    @Produces("application/json")
    @Path("/list")
    @GET
    public Response listNodes(@QueryParam("clazz") @DefaultValue("java.lang.String") String clazzString, @QueryParam("path") String path) {
        try {
            Preconditions.checkNotNull(path);

            Class clazz = Class.forName(clazzString);

            Set<Node> nodes = nodeKeeper.listChildrenNodes(path,clazz);
            HashMap<String,NodeWrapper> nodeMap = new HashMap<String, NodeWrapper>();

            for(Node node : nodes) {
                int nbOfChildren = nodeKeeper.nbOfChildren(node.getPath());
                nodeMap.put(node.getPath().substring(node.getPath().lastIndexOf("/") + 1),new NodeWrapper(node,nbOfChildren,clazzString));
            }

            return Response.ok().entity(nodeMap).build();
        } catch (ClassNotFoundException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(String.format("cannot get class for %s",clazzString)).build();
        } catch (NullPointerException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("parameters must be set").build();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return Response.serverError().entity("cannot connect zookeeper").build();
        } catch (NodeKeeperException e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).entity(String.format("cannot parse nodedata to %s: %s",clazzString,e.getMessage())).build();
        }

    }

    @POST
    @Produces("application/json")
    public Response createNode(@QueryParam("path")String path, @QueryParam("clazz") @DefaultValue("java.lang.String") String clazzString, @Context HttpServletRequest request) {
        try {
            Preconditions.checkNotNull(path);
            Class clazz = Class.forName(clazzString);

            DataHandler handler = nodeKeeper.getDataHandler(clazz);

            Preconditions.checkNotNull(handler);

            Object o = handler.parse(IOUtils.toByteArray(request.getInputStream()));
            Node node = new Node<Object>(path,o);

            nodeKeeper.writeNode(node,clazz);

            return Response.ok().entity(new NodeWrapper(node,0,clazzString)).build();

        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("something happend - TODO").build();
        }
    }

    @GET
    @Produces("application/json")
    public Response readNode(@QueryParam("path")String path, @QueryParam("clazz") @DefaultValue("java.lang.String") String clazzString) {
        try {
            Preconditions.checkNotNull(path);
            Class clazz = Class.forName(clazzString);

            Node node = nodeKeeper.readNode(path,clazz);
            int nbOfChildren = nodeKeeper.nbOfChildren(node.getPath());

            return Response.ok().entity(new NodeWrapper(node,nbOfChildren,clazzString)).build();

        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("something happend - TODO").build();
        }
    }

    @PUT
    @Produces("application/json")
    public Response updateNode(@QueryParam("path")String path, @QueryParam("clazz") @DefaultValue("java.lang.String") String clazzString, @Context HttpServletRequest request) {
        return createNode(path,clazzString,request);
    }

    @DELETE
    public Response deleteNode(@QueryParam("path")String path) {
        try {
            Preconditions.checkNotNull(path);

            cleanNode(new Node<Object>(path));

            return Response.ok().build();

        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("something happend - TODO").build();
        }
    }

    private void cleanNode(Node node) throws NodeKeeperException, InterruptedException {
        for(Node child : nodeKeeper.listChildrenNodes(node.getPath(),String.class)) {
            cleanNode(child);
        }
        try {
            nodeKeeper.deleteNode(node);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
