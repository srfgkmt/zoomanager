package at.salzburgresearch.zookeeper.manager.webservice;

import at.salzburgresearch.nodekeeper.NodeKeeper;
import at.salzburgresearch.nodekeeper.eca.Rule;
import at.salzburgresearch.nodekeeper.eca.RuleHandler;
import at.salzburgresearch.nodekeeper.exception.NodeKeeperException;
import at.salzburgresearch.zookeeper.manager.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
@Path("/service/nodekeeper")
public class NKRuleWebservice {

    Logger logger = LoggerFactory.getLogger(NKRuleWebservice.class);

    public NKRuleWebservice(NodeKeeper nodekeeper) {
        Constants.ruleHandler = new RuleHandler(nodekeeper);
        //read rules
        File file = new File(Constants.ZooManagerHome+File.separator+Constants.RULE_FILE);
        if(file.exists() && file.isFile()) {
            try {
                Constants.ruleHandler.readRules(new FileInputStream(file));
            } catch (IOException e) {
                logger.error("cannot read rules for zoomanager");
            } catch (InterruptedException e) {
                logger.error("cannot read rules for zoomanager");
            } catch (NodeKeeperException e) {
                logger.error("cannot read rules for zoomanager");
            }
        }
    }

    @GET
    @Path("/rules.xml")
    @Produces("application/xml")
    public Response readRules() {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            Constants.ruleHandler.writeRules(baos);
            String result = baos.toString();
            baos.close();
            return Response.ok().entity(result).build();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (TransformerException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return Response.serverError().build();
    }

    @GET
    @Path("/rule-ids")
    @Produces("application/json")
    public Response listRuleIds() {

        HashMap<String,String> idPlusDescription = new HashMap<>();

        for(String id : Constants.ruleHandler.getRuleIds()) {
            idPlusDescription.put(id,Constants.ruleHandler.getRule(id).getDescription());
        }

        return Response.ok().entity(idPlusDescription).build();
    }

    @POST
    @Path("/rules")
    @Consumes("application/xml")
    public Response writeRules(@Context HttpServletRequest request) {
        try {
            Constants.ruleHandler.readRules(request.getInputStream());
            //TODO write to home
            return Response.ok().build();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        } catch (NodeKeeperException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Response.serverError().entity("cannot store rules").build();
    }

    @GET
    @Path("/rule")
    @Produces("application/xml")
    public Response readRule(@QueryParam("id")String id) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            List<Rule> rules = new ArrayList<>();
            rules.add(Constants.ruleHandler.getRule(id));
            RuleHandler.InputOutputHandler.serializeRules(rules,baos);
            String result = baos.toString();
            baos.close();
            return Response.ok().entity(result).build();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (TransformerException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return Response.serverError().build();
    }

    @DELETE
    @Path("/rule")
    @Produces("application/xml")
    public Response deleteRule(@QueryParam("id")String id) {
        Rule rule = Constants.ruleHandler.getRule(id);
        if(rule != null) Constants.ruleHandler.removeRule(rule);
        return Response.ok().build();
    }

    @PUT
    @Path("/rules")
    @Consumes("application/xml")
    public Response addRule(@Context HttpServletRequest request) {
        try {
            List<Rule> rules = RuleHandler.InputOutputHandler.parseRules(request.getInputStream());
            for(Rule rule : rules) {
                Constants.ruleHandler.addRule(rule);
            }
            return Response.ok().build();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (NodeKeeperException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return Response.serverError().entity("cannot store rules").build();
    }

}
