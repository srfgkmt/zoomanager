package at.salzburgresearch.zookeeper.manager.webservice;

import at.salzburgresearch.nodekeeper.NodeKeeper;
import at.salzburgresearch.nodekeeper.bootstrap.ZKBootstrap;
import at.salzburgresearch.nodekeeper.exception.NodeKeeperException;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.*;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
@Path("/service/zookeeper/bootstrap")
public class ZKBootstrapWebservice {

    private ZKBootstrap zkBootstrap;

    public ZKBootstrapWebservice(NodeKeeper nodeKeeper) {
        zkBootstrap = new ZKBootstrap(nodeKeeper);
    }

    @POST
    @Path("/upload")
    public Response uploadFile(@QueryParam("file") String file, @QueryParam("clean")@DefaultValue("true") boolean clean) throws IOException {

        try {
            zkBootstrap.load(new File(file),clean);
            return Response.status(200).entity("Uploaded file").build();
        } catch (NodeKeeperException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return Response.serverError().build();
    }


    @GET
    @Path("/zookeeper.properties")
    @Produces("plain/text")
    public Response getFile() {
        StreamingOutput stream = new StreamingOutput() {
            @Override
            public void write(OutputStream os) throws IOException, WebApplicationException {
                try {
                    zkBootstrap.write(os);
                } catch (NodeKeeperException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        return Response.ok(stream).build();
    }
}
