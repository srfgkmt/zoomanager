package at.salzburgresearch.zookeeper.manager.utils;

import at.salzburgresearch.nodekeeper.NodeKeeper;
import at.salzburgresearch.nodekeeper.eca.RuleHandler;

import java.util.Properties;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
public class Constants {

    public static final String RULE_FILE = "rules.xml";
    public static final String PROPERTIES_FILE = "properties.properties";

    //public static String ZKConnectionString = "zookeeper1.bb.redlink.io:2181,zookeeper2.bb.redlink.io:2181,zookeeper3.bb.redlink.io:2181";
    public static String ZKConnectionString = "localhost:2181";
    public static int ZKTimeout = 60000;

    public static String ZooManagerHome = "/tmp/zoomanager";

    public static Properties NodeKeeperProperties = new Properties();
    public static RuleHandler ruleHandler = null;

}
