package at.salzburgresearch.zookeeper.manager.model;

import at.salzburgresearch.nodekeeper.model.Node;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
public class NodeWrapper {

    private String path;
    private Object data;
    private int version;
    private int nbOfChildren;
    private String clazz;

    public NodeWrapper(Node node, int nbOfChildren, String clazz) {
        this.path = node.getPath();
        this.data = node.getData();
        this.version = node.getVersion();
        this.nbOfChildren = nbOfChildren;
        this.clazz = clazz;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getNbOfChildren() {
        return nbOfChildren;
    }

    public void setNbOfChildren(int nbOfChildren) {
        this.nbOfChildren = nbOfChildren;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }
}
