package at.salzburgresearch.zookeeper.manager.websocket;

import at.salzburgresearch.nodekeeper.NodeKeeper;
import at.salzburgresearch.nodekeeper.NodeListener;
import at.salzburgresearch.nodekeeper.exception.NodeKeeperException;
import at.salzburgresearch.nodekeeper.model.Node;
import at.salzburgresearch.zookeeper.manager.utils.Constants;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */

@ServerEndpoint("/websocket/nodes")
public class ZKNodeWebSocketNew {

    private Properties properties = new Properties();
    private static NodeKeeper nodeKeeper;

    private Session session;
    private static final Set<ZKNodeWebSocketNew> connections = new CopyOnWriteArraySet<ZKNodeWebSocketNew>();

    public ZKNodeWebSocketNew() throws InterruptedException, IOException, NodeKeeperException {
        nodeKeeper = new NodeKeeper(Constants.ZKConnectionString,Constants.ZKTimeout,properties);
        nodeKeeper.addListener("/.+",new AllNodeListener());
        nodeKeeper.startListeners();
    }

    @OnOpen
    public void onOpen(Session session) throws InterruptedException, IOException, NodeKeeperException {
        this.session = session;
        connections.add(this);
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        connections.remove(this);
    }

    private static void writeMessage(String msg) {
        for (ZKNodeWebSocketNew client : connections) {
            try {
                synchronized (client) {
                    client.session.getBasicRemote().sendText(msg);
                }
            } catch (IOException e) {
                connections.remove(client);
                try {
                    client.session.close();
                } catch (IOException e1) {
                    // Ignore
                }
                writeMessage("client has been disconnected");
            }
        }
    }

    class AllNodeListener extends NodeListener<String> {

        @Override
        public void onNodeCreated(Node<String> stringNode) throws InterruptedException, NodeKeeperException {
            writeMessage("created "+stringNode.getPath());
        }

        @Override
        public void onNodeUpdated(Node<String> stringNode) throws InterruptedException, NodeKeeperException {
            writeMessage("updated "+stringNode.getPath());
        }

        @Override
        public void onNodeDeleted(Node<String> stringNode) throws InterruptedException, NodeKeeperException {
            writeMessage("deleted "+stringNode.getPath());
        }

        @Override
        public Class<String> getType() {
            return String.class;
        }

    }

}
