package at.salzburgresearch.zookeeper.manager.websocket;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Properties;

import at.salzburgresearch.nodekeeper.NodeKeeper;
import at.salzburgresearch.nodekeeper.NodeListener;
import at.salzburgresearch.nodekeeper.exception.NodeKeeperException;
import at.salzburgresearch.nodekeeper.model.Node;
import at.salzburgresearch.zookeeper.manager.utils.Constants;
import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WebSocketServlet;
import org.apache.catalina.websocket.WsOutbound;

import javax.servlet.http.HttpServletRequest;

public class ZKNodeWebSocket extends WebSocketServlet {
    private static final long serialVersionUID = 1L;
    private static ArrayList<MyMessageInbound> mmiList = new ArrayList<MyMessageInbound>();

    private Properties properties;
    private static NodeKeeper nodeKeeper;

    @Override
    protected StreamInbound createWebSocketInbound(String s, HttpServletRequest httpServletRequest) {
        return new MyMessageInbound();
    }

    private class MyMessageInbound extends MessageInbound{
        WsOutbound myoutbound;

        private Properties readProperties() {
            //TODO read
            return new Properties();
        }

        private void writeProperties(Properties properties) {
            //TODO write
        }

        @Override
        public void onOpen(WsOutbound outbound){
            try {
                this.myoutbound = outbound;
                mmiList.add(this);

                properties = readProperties();

                nodeKeeper = new NodeKeeper(Constants.ZKConnectionString,Constants.ZKTimeout,properties);
                nodeKeeper.addListener("/.+",new AllNodeListener());
                nodeKeeper.startListeners();

                outbound.writeTextMessage(CharBuffer.wrap("activate"));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (NodeKeeperException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        @Override
        public void onClose(int status){
            mmiList.remove(this);
            try {
                writeProperties(properties);
                nodeKeeper.shutdown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onTextMessage(CharBuffer cb) throws IOException{

        }

        @Override
        public void onBinaryMessage(ByteBuffer bb) throws IOException{

        }

        class AllNodeListener extends NodeListener<String> {

            @Override
            public void onNodeCreated(Node<String> stringNode) throws InterruptedException, NodeKeeperException {
                try {
                    writeMessage("created "+stringNode.getPath());
                } catch (IOException e) {
                    throw new NodeKeeperException("cannot write message");
                }
            }

            @Override
            public void onNodeUpdated(Node<String> stringNode) throws InterruptedException, NodeKeeperException {
                try {
                    writeMessage("updated "+stringNode.getPath());
                } catch (IOException e) {
                    throw new NodeKeeperException("cannot write message");
                }
            }

            @Override
            public void onNodeDeleted(Node<String> stringNode) throws InterruptedException, NodeKeeperException {
                try {
                    writeMessage("deleted "+stringNode.getPath());
                } catch (IOException e) {
                    throw new NodeKeeperException("cannot write message");
                }
            }

            @Override
            public Class<String> getType() {
                return String.class;
            }

            private void writeMessage(String s) throws IOException {
                for(MyMessageInbound mmib: mmiList){
                    if(s != null) {
                        CharBuffer buffer = CharBuffer.wrap(s);
                        mmib.myoutbound.writeTextMessage(buffer);
                        mmib.myoutbound.flush();
                    }
                }
            }
        }
    }
}