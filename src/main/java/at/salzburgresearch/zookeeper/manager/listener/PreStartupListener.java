package at.salzburgresearch.zookeeper.manager.listener;

import at.salzburgresearch.zookeeper.manager.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.util.Date;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
public class PreStartupListener implements ServletContextListener {

    Logger logger = LoggerFactory.getLogger(PreStartupListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        logger.info("init application");

        String host = servletContextEvent.getServletContext().getInitParameter("zoomanager.host");
        String timeout = servletContextEvent.getServletContext().getInitParameter("zoomanager.timeout");
        String home = servletContextEvent.getServletContext().getInitParameter("zoomanager.home");

        if(host != null) {
            Constants.ZKConnectionString = host;
        }
        if(timeout != null) try {
            Constants.ZKTimeout = Integer.valueOf(timeout);
        } catch (Exception e) {
            logger.error("zoomanager.timeout cannot be set. Not an integer?");
        }
        if(home != null) Constants.ZooManagerHome = home;

        //test if home exists
        File homedir = new File(Constants.ZooManagerHome);
        if(!homedir.exists())  {
            homedir.mkdirs();
        }

        //read properties (if there are one)
        File file = new File(Constants.ZooManagerHome+File.separator+Constants.PROPERTIES_FILE);
        if(file.exists() && file.isFile()) {
            try {
                Constants.NodeKeeperProperties.load(new FileInputStream(file));
            } catch (IOException e) {
                logger.error("Cannot read property file for zoomanager");
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        File f = new File(Constants.ZooManagerHome+File.separator+Constants.PROPERTIES_FILE);
        try {
            OutputStream out = new FileOutputStream( f );
            Constants.NodeKeeperProperties.store(out,"stored properties on " + new Date().toString());
        } catch (FileNotFoundException e) {
            logger.error("Cannot write property file for zoomanager");
        } catch (IOException e) {
            logger.error("Cannot write property file for zoomanager");
        }

        if(Constants.ruleHandler != null) {
            File rf = new File(Constants.ZooManagerHome+File.separator+Constants.RULE_FILE);
            try {
                OutputStream out = new FileOutputStream( rf );
                Constants.ruleHandler.writeRules(out);
            } catch (FileNotFoundException e) {
                logger.error("Cannot write rule file for zoomanager");
            } catch (IOException e) {
                logger.error("Cannot write rule file for zoomanager");
            } catch (ParserConfigurationException e) {
                logger.error("Cannot write rule file for zoomanager");
            } catch (TransformerException e) {
                logger.error("Cannot write rule file for zoomanager");
            }
        }
    }
}
